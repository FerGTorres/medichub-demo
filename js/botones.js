document.querySelector("#registro").addEventListener("mouseenter", mostrarGifR);
document.querySelector("#registro").addEventListener("mouseleave", ocultarGifR);
document.querySelector("#login").addEventListener("mouseenter", mostrarGifL);
document.querySelector("#login").addEventListener("mouseleave", ocultarGifL);
document.querySelector("#inicio").addEventListener("mouseenter", mostrarGifI);
document.querySelector("#inicio").addEventListener("mouseleave", ocultarGifI);

var gifRegistro = document.querySelector("#gif-registro");
var gifLogin = document.querySelector("#gif-login");
var gifInicio = document.querySelector("#gif-inicio");

function mostrarGifR() {
    gifRegistro.style.display = "block";
}

function ocultarGifR() { 
    gifRegistro.style.display = "none";
}

function mostrarGifL() {
    gifLogin.style.display = "block";
}

function ocultarGifL() { 
    gifLogin.style.display = "none";
}

function mostrarGifI() {
    gifInicio.style.display = "block";
}

function ocultarGifI() { 
    gifInicio.style.display = "none";
}